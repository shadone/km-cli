//
//  main.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import Foundation
import ArgumentParser
import ApolloCombine

// let developerToken = "XXX"
let kitemakerApi = KitemakerApi(token: developerToken)

struct KitemakerCLI: ParsableCommand {
    static var configuration = CommandConfiguration(
        abstract: "A utility for accessing Kitemaker API.",
        version: "1.0.0",
        subcommands: [
            ListSpaces.self,
            ListItems.self,
        ]
    )
}

KitemakerCLI.main()
