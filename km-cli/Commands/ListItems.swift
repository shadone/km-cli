//
//  ListItems.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import ArgumentParser
import Foundation

extension KitemakerCLI {
    struct ListItems: ParsableCommand {
        static var configuration =
            CommandConfiguration(
                commandName: "list-items",
                abstract: "List work items."
            )

        mutating func run() {
            do {
                let result = try kitemakerApi.sync(SpaceQueryQuery())
                guard let data = result.data else { fatalError("missing response data") }

                for space in data.organization.spaces {
                    print("\n\nSpace: \(space.name)")

                    print("\("Status".padded(to: 30))\("Key".padded(to: 20))Title")

                    let result = try kitemakerApi.sync(ItemsQueryQuery(spaceId: space.id))
                    guard let data = result.data else { fatalError("missing response data") }
                    for item in data.workItems.workItems {

                        let status = item.status.name
                        let itemNumber = "\(space.key)-\(item.number)"
                        let title = item.title
                        print("\(status.padded(to: 30))\(itemNumber.padded(to: 20))\(title)")
                    }
                }
            } catch {
                print(error)
            }
        }
    }
}
