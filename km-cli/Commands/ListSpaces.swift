//
//  ListSpaces.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import ArgumentParser
import Foundation

extension KitemakerCLI {
    struct ListSpaces: ParsableCommand {
        static var configuration =
            CommandConfiguration(
                commandName: "list-spaces",
                abstract: "List Spaces."
            )

        mutating func run() {
            do {
                let result = try kitemakerApi.sync(SpaceQueryQuery())
                guard let data = result.data else { fatalError("missing response data") }

                let organizationName = data.organization.name

                print(organizationName)
                print("Key            Space name")
                for space in data.organization.spaces {
                    print("\(space.key.padding(toLength: 15, withPad: " ", startingAt: 0))\(space.name)")
                }
            } catch {
                print(error)
            }
        }
    }
}
