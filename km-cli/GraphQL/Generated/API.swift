// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

/// Statuses for [WorkItem](#workitem) objects fall into a few different categories
public enum StatusType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case backlog
  case todo
  case inProgress
  case done
  case archived
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "BACKLOG": self = .backlog
      case "TODO": self = .todo
      case "IN_PROGRESS": self = .inProgress
      case "DONE": self = .done
      case "ARCHIVED": self = .archived
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .backlog: return "BACKLOG"
      case .todo: return "TODO"
      case .inProgress: return "IN_PROGRESS"
      case .done: return "DONE"
      case .archived: return "ARCHIVED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: StatusType, rhs: StatusType) -> Bool {
    switch (lhs, rhs) {
      case (.backlog, .backlog): return true
      case (.todo, .todo): return true
      case (.inProgress, .inProgress): return true
      case (.done, .done): return true
      case (.archived, .archived): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [StatusType] {
    return [
      .backlog,
      .todo,
      .inProgress,
      .done,
      .archived,
    ]
  }
}

public final class OrgQueryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query OrgQuery {
      organization {
        __typename
        id
        name
      }
    }
    """

  public let operationName: String = "OrgQuery"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("organization", type: .nonNull(.object(Organization.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(organization: Organization) {
      self.init(unsafeResultMap: ["__typename": "Query", "organization": organization.resultMap])
    }

    /// Fetch the main organization object. Useful for finding out which users, spaces, labels, statuses, etc. are available before fetching and manipulating work items and themes
    public var organization: Organization {
      get {
        return Organization(unsafeResultMap: resultMap["organization"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "organization")
      }
    }

    public struct Organization: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Organization"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, name: String) {
        self.init(unsafeResultMap: ["__typename": "Organization", "id": id, "name": name])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class SpaceQueryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query SpaceQuery {
      organization {
        __typename
        id
        name
        spaces {
          __typename
          id
          key
          name
          labels {
            __typename
            id
            name
            color
          }
          statuses {
            __typename
            id
            name
            type
            default
          }
        }
      }
    }
    """

  public let operationName: String = "SpaceQuery"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("organization", type: .nonNull(.object(Organization.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(organization: Organization) {
      self.init(unsafeResultMap: ["__typename": "Query", "organization": organization.resultMap])
    }

    /// Fetch the main organization object. Useful for finding out which users, spaces, labels, statuses, etc. are available before fetching and manipulating work items and themes
    public var organization: Organization {
      get {
        return Organization(unsafeResultMap: resultMap["organization"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "organization")
      }
    }

    public struct Organization: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Organization"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("spaces", type: .nonNull(.list(.nonNull(.object(Space.selections))))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, name: String, spaces: [Space]) {
        self.init(unsafeResultMap: ["__typename": "Organization", "id": id, "name": name, "spaces": spaces.map { (value: Space) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var spaces: [Space] {
        get {
          return (resultMap["spaces"] as! [ResultMap]).map { (value: ResultMap) -> Space in Space(unsafeResultMap: value) }
        }
        set {
          resultMap.updateValue(newValue.map { (value: Space) -> ResultMap in value.resultMap }, forKey: "spaces")
        }
      }

      public struct Space: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Space"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("key", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
            GraphQLField("labels", type: .nonNull(.list(.nonNull(.object(Label.selections))))),
            GraphQLField("statuses", type: .nonNull(.list(.nonNull(.object(Status.selections))))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, key: String, name: String, labels: [Label], statuses: [Status]) {
          self.init(unsafeResultMap: ["__typename": "Space", "id": id, "key": key, "name": name, "labels": labels.map { (value: Label) -> ResultMap in value.resultMap }, "statuses": statuses.map { (value: Status) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var key: String {
          get {
            return resultMap["key"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "key")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var labels: [Label] {
          get {
            return (resultMap["labels"] as! [ResultMap]).map { (value: ResultMap) -> Label in Label(unsafeResultMap: value) }
          }
          set {
            resultMap.updateValue(newValue.map { (value: Label) -> ResultMap in value.resultMap }, forKey: "labels")
          }
        }

        public var statuses: [Status] {
          get {
            return (resultMap["statuses"] as! [ResultMap]).map { (value: ResultMap) -> Status in Status(unsafeResultMap: value) }
          }
          set {
            resultMap.updateValue(newValue.map { (value: Status) -> ResultMap in value.resultMap }, forKey: "statuses")
          }
        }

        public struct Label: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Label"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
              GraphQLField("name", type: .nonNull(.scalar(String.self))),
              GraphQLField("color", type: .nonNull(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID, name: String, color: String) {
            self.init(unsafeResultMap: ["__typename": "Label", "id": id, "name": name, "color": color])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return resultMap["id"]! as! GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var color: String {
            get {
              return resultMap["color"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "color")
            }
          }
        }

        public struct Status: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Status"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
              GraphQLField("name", type: .nonNull(.scalar(String.self))),
              GraphQLField("type", type: .nonNull(.scalar(StatusType.self))),
              GraphQLField("default", type: .nonNull(.scalar(Bool.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID, name: String, type: StatusType, `default`: Bool) {
            self.init(unsafeResultMap: ["__typename": "Status", "id": id, "name": name, "type": type, "default": `default`])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return resultMap["id"]! as! GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var type: StatusType {
            get {
              return resultMap["type"]! as! StatusType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var `default`: Bool {
            get {
              return resultMap["default"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "default")
            }
          }
        }
      }
    }
  }
}

public final class ItemsQueryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ItemsQuery($spaceId: ID!, $cursor: String) {
      workItems(spaceId: $spaceId, cursor: $cursor) {
        __typename
        cursor
        hasMore
        workItems {
          __typename
          id
          number
          title
          status {
            __typename
            id
            name
            type
          }
          labels {
            __typename
            id
            name
            color
          }
        }
      }
    }
    """

  public let operationName: String = "ItemsQuery"

  public var spaceId: GraphQLID
  public var cursor: String?

  public init(spaceId: GraphQLID, cursor: String? = nil) {
    self.spaceId = spaceId
    self.cursor = cursor
  }

  public var variables: GraphQLMap? {
    return ["spaceId": spaceId, "cursor": cursor]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("workItems", arguments: ["spaceId": GraphQLVariable("spaceId"), "cursor": GraphQLVariable("cursor")], type: .nonNull(.object(WorkItem.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(workItems: WorkItem) {
      self.init(unsafeResultMap: ["__typename": "Query", "workItems": workItems.resultMap])
    }

    /// Fetch [WorkItem](#workitem) objects for a particular [Space](#space). This will return at most 50 work items at a time so use the paging facilities to fetch more results
    public var workItems: WorkItem {
      get {
        return WorkItem(unsafeResultMap: resultMap["workItems"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "workItems")
      }
    }

    public struct WorkItem: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["WorkItemsQueryResult"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("cursor", type: .nonNull(.scalar(String.self))),
          GraphQLField("hasMore", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("workItems", type: .nonNull(.list(.nonNull(.object(WorkItem.selections))))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(cursor: String, hasMore: Bool, workItems: [WorkItem]) {
        self.init(unsafeResultMap: ["__typename": "WorkItemsQueryResult", "cursor": cursor, "hasMore": hasMore, "workItems": workItems.map { (value: WorkItem) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// An opaque string used for paging. Pass this string to the workItems query to iterate through the results a page at a time
      public var cursor: String {
        get {
          return resultMap["cursor"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "cursor")
        }
      }

      /// Indicates if more results are available (by requerying workItems, passing the cursor)
      public var hasMore: Bool {
        get {
          return resultMap["hasMore"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "hasMore")
        }
      }

      public var workItems: [WorkItem] {
        get {
          return (resultMap["workItems"] as! [ResultMap]).map { (value: ResultMap) -> WorkItem in WorkItem(unsafeResultMap: value) }
        }
        set {
          resultMap.updateValue(newValue.map { (value: WorkItem) -> ResultMap in value.resultMap }, forKey: "workItems")
        }
      }

      public struct WorkItem: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["WorkItem"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("number", type: .nonNull(.scalar(String.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("status", type: .nonNull(.object(Status.selections))),
            GraphQLField("labels", type: .nonNull(.list(.nonNull(.object(Label.selections))))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, number: String, title: String, status: Status, labels: [Label]) {
          self.init(unsafeResultMap: ["__typename": "WorkItem", "id": id, "number": number, "title": title, "status": status.resultMap, "labels": labels.map { (value: Label) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var number: String {
          get {
            return resultMap["number"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "number")
          }
        }

        public var title: String {
          get {
            return resultMap["title"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var status: Status {
          get {
            return Status(unsafeResultMap: resultMap["status"]! as! ResultMap)
          }
          set {
            resultMap.updateValue(newValue.resultMap, forKey: "status")
          }
        }

        public var labels: [Label] {
          get {
            return (resultMap["labels"] as! [ResultMap]).map { (value: ResultMap) -> Label in Label(unsafeResultMap: value) }
          }
          set {
            resultMap.updateValue(newValue.map { (value: Label) -> ResultMap in value.resultMap }, forKey: "labels")
          }
        }

        public struct Status: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Status"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
              GraphQLField("name", type: .nonNull(.scalar(String.self))),
              GraphQLField("type", type: .nonNull(.scalar(StatusType.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID, name: String, type: StatusType) {
            self.init(unsafeResultMap: ["__typename": "Status", "id": id, "name": name, "type": type])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return resultMap["id"]! as! GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var type: StatusType {
            get {
              return resultMap["type"]! as! StatusType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }
        }

        public struct Label: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Label"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
              GraphQLField("name", type: .nonNull(.scalar(String.self))),
              GraphQLField("color", type: .nonNull(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID, name: String, color: String) {
            self.init(unsafeResultMap: ["__typename": "Label", "id": id, "name": name, "color": color])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return resultMap["id"]! as! GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var color: String {
            get {
              return resultMap["color"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "color")
            }
          }
        }
      }
    }
  }
}

public final class CreateWorkItemMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation CreateWorkItem($statusId: ID!, $title: String!, $description: String) {
      createWorkItem(
        input: {statusId: $statusId, title: $title, description: $description}
      ) {
        __typename
        workItem {
          __typename
          id
          number
        }
      }
    }
    """

  public let operationName: String = "CreateWorkItem"

  public var statusId: GraphQLID
  public var title: String
  public var description: String?

  public init(statusId: GraphQLID, title: String, description: String? = nil) {
    self.statusId = statusId
    self.title = title
    self.description = description
  }

  public var variables: GraphQLMap? {
    return ["statusId": statusId, "title": title, "description": description]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createWorkItem", arguments: ["input": ["statusId": GraphQLVariable("statusId"), "title": GraphQLVariable("title"), "description": GraphQLVariable("description")]], type: .nonNull(.object(CreateWorkItem.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createWorkItem: CreateWorkItem) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createWorkItem": createWorkItem.resultMap])
    }

    public var createWorkItem: CreateWorkItem {
      get {
        return CreateWorkItem(unsafeResultMap: resultMap["createWorkItem"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "createWorkItem")
      }
    }

    public struct CreateWorkItem: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CreateWorkItemResult"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("workItem", type: .nonNull(.object(WorkItem.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(workItem: WorkItem) {
        self.init(unsafeResultMap: ["__typename": "CreateWorkItemResult", "workItem": workItem.resultMap])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var workItem: WorkItem {
        get {
          return WorkItem(unsafeResultMap: resultMap["workItem"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "workItem")
        }
      }

      public struct WorkItem: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["WorkItem"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("number", type: .nonNull(.scalar(String.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, number: String) {
          self.init(unsafeResultMap: ["__typename": "WorkItem", "id": id, "number": number])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var number: String {
          get {
            return resultMap["number"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "number")
          }
        }
      }
    }
  }
}
