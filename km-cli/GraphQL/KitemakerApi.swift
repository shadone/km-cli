//
//  KitemakerApi.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import Apollo
import Foundation

class KitemakerApi {
    let endpointUrl = URL(string: "https://toil.kitemaker.co/developers/graphql")!
    let apollo: ApolloClient

    init(token: String) {
        let store = ApolloStore(cache: InMemoryNormalizedCache())
        let provider = LegacyInterceptorProvider(store: store)
        let transport = RequestChainNetworkTransport(
            interceptorProvider: provider,
            endpointURL: endpointUrl,
            additionalHeaders: [
                "Authorization": "Bearer \(token)",
            ]
        )
        apollo = ApolloClient(networkTransport: transport, store: store)
    }

    func sync<Query: GraphQLQuery>(_ query: Query) throws -> GraphQLResult<Query.Data> {
        var maybeResult: Result<GraphQLResult<Query.Data>, Error>?

        _ = apollo.fetch(query: query) { result in
            maybeResult = result
        }

        while maybeResult == nil {
            let soon = Date().addingTimeInterval(1)
            RunLoop.main.run(until: soon)
        }

        switch maybeResult! {
        case let .success(value):
            return value

        case let .failure(error):
            throw error
        }
    }
}
