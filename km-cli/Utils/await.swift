//
//  await.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import Combine
import Foundation

func await<Value, Error>(_ publisher: AnyPublisher<Value, Error>) throws -> Value {
    try publisher.await()
}

extension Publisher {
    func await() throws -> Output {
        let semaphore = DispatchSemaphore(value: 0)

        var maybeResult: Result<Output, Failure>?

        var disposables = Set<AnyCancellable>()

        sink { complete in
            switch complete {
            case .finished:
                break
            case let .failure(error):
                maybeResult = .failure(error)
            }
            semaphore.signal()
        } receiveValue: { value in
            maybeResult = .success(value)
        }
        .store(in: &disposables)

        semaphore.wait()

        guard let result = maybeResult else {
            preconditionFailure("Got neither value nor error")
        }
        return try result.get()
    }
}
