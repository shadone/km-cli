//
//  String+padded.swift
//  km-cli
//
//  Created by Denis Dzyubenko on 29/04/2021.
//

import Foundation

extension String {
    func padded(to length: Int) -> String {
        padding(toLength: length, withPad: " ", startingAt: 0)
    }
}
